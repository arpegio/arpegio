"""Tests for the categories views."""
from django.core.urlresolvers import reverse

from arpegio.categories.models import Category
from arpegio.categories.views import CategoryDetail


def test_category_view(rf, mocker):
    """The category detail view should return a 200 status."""
    get_object_mock = mocker.patch(
        'arpegio.categories.views.CategoryDetail.get_object')
    category = Category()
    category.slug = 'a-category'
    get_object_mock.return_value = category
    request = rf.get(reverse('categories:category',
                             kwargs={'slug': category.slug}
                             )
                     )
    response = CategoryDetail.as_view()(request, slug=category.slug)
    assert response.status_code == 200


def test_category_view_template_name(mocker):
    """The template names include 'arpegio-categories/category_detail.html'."""
    view = CategoryDetail()
    view.object = mocker
    templates = view.get_template_names()
    assert 'arpegio-categories/category_detail.html' in templates
