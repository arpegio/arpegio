"""Tests the categories' models."""
from django.core.urlresolvers import reverse

from arpegio.categories.models import Category


def test_category_str():
    """Test the category string representation is its name."""
    category = Category(name='Category')
    assert str(category) == 'Category'


def test_category_url():
    """Get the url of the category."""
    category = Category(name='A category', slug='a-category')
    assert category.get_absolute_url() == reverse('categories:category',
                                                  kwargs={
                                                      'slug': category.slug
                                                  }
                                                  )
