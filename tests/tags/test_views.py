"""Tests for the tags views."""
from django.core.urlresolvers import reverse

from arpegio.tags.models import Tag
from arpegio.tags.views import TagDetail


def test_tag_view(rf, mocker):
    """The tag detail view should return a 200 status."""
    get_object_mock = mocker.patch('arpegio.tags.views.TagDetail.get_object')
    tag = Tag()
    tag.slug = 'a-tag'
    get_object_mock.return_value = tag
    request = rf.get(reverse('tags:tag', kwargs={'slug': tag.slug}))
    response = TagDetail.as_view()(request, slug=tag.slug)
    assert response.status_code == 200


def test_category_view_template_name(mocker):
    """The template names include 'arpegio-tags/tag_detail.html'."""
    view = TagDetail()
    view.object = mocker
    templates = view.get_template_names()
    assert 'arpegio-tags/tag_detail.html' in templates
