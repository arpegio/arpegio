"""Tests the tags' models."""
from django.core.urlresolvers import reverse

from arpegio.tags.models import Tag


def test_tag_str():
    """Test the tag string representation is its name."""
    tag = Tag(name='Tag')
    assert str(tag) == 'Tag'


def test_category_url():
    """Get the url of the tag."""
    tag = Tag(name='A tag', slug='a-tag')
    assert tag.get_absolute_url() == reverse('tags:tag',
                                             kwargs={'slug': tag.slug}
                                             )
