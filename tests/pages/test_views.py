"""Tests for the pages views."""
from django.core.urlresolvers import reverse

from arpegio.pages.views import PageDetail
from arpegio.pages.models import Page


def test_page_view(rf, mocker):
    """The page detail view should return a 200 status."""
    get_object_mock = mocker.patch('arpegio.pages.views.PageDetail.get_object')
    page = Page()
    page.slug = 'a-page'
    get_object_mock.return_value = page
    request = rf.get(reverse('pages:page', kwargs={'slug': page.slug}))
    response = PageDetail.as_view()(request, slug=page.slug)
    assert response.status_code == 200


def test_page_view_template_name(mocker):
    """The template names include 'arpegio-pages/page_detail.html'."""
    view = PageDetail()
    view.object = mocker
    templates = view.get_template_names()
    assert 'arpegio-pages/page_detail.html' in templates
