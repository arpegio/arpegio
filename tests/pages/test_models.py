"""Tests for the pages models."""
from django.core.urlresolvers import reverse

from arpegio.pages.models import Page


def test_page_url():
    """Get the url of the page."""
    page = Page(title='A title', slug='a-title')
    assert page.get_absolute_url() == reverse('pages:page',
                                              kwargs={'slug': page.slug}
                                              )
