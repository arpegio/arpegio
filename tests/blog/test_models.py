"""Tests for the blog models."""
from django.core.urlresolvers import reverse

from arpegio.blog.models import Post


def test_post_url():
    """Get the url of the post."""
    post = Post(title='A title', slug='a-title')
    assert post.get_absolute_url() == reverse('blog:post',
                                              kwargs={'slug': post.slug}
                                              )
