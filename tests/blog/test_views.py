"""Tests for the blog views."""
from model_mommy import mommy

from django.core.urlresolvers import reverse

from arpegio.blog.views import PostList
from arpegio.blog.views import PostDetail
from arpegio.blog.models import Post


def test_post_list_view(rf, mocker):
    """The post list view should return a 200 status."""
    mocker.patch('arpegio.blog.views.PostList.get_queryset')
    mocker.patch('arpegio.blog.views.PostList.get_context_data')
    request = rf.get(reverse('blog:blog'))
    response = PostList.as_view()(request)
    assert response.status_code == 200


def test_post_list_queryset(mocker):
    """The queryset should combine sticky and public posts."""
    public_mock = mocker.patch('arpegio.blog.views.Post.objects.public')
    sticky_mock = mocker.patch('arpegio.blog.views.Post.objects.sticky')
    instance = PostList()
    PostList.get_queryset(instance)
    public_mock.assert_called_once_with()
    sticky_mock.assert_called_once_with()


def test_post_list_pagination(rf, mocker):
    """The post list view should paginate the content."""
    public_mock = mocker.patch('arpegio.blog.views.Post.objects.public')
    mocker.patch('arpegio.blog.views.Post.objects.sticky')
    posts = mommy.prepare(Post, _quantity=20)
    public_mock.return_value = posts
    request = rf.get(reverse('blog:blog'))
    response = PostList.as_view()(request)
    paginated = response.context_data['is_paginated']
    assert paginated is True


def test_post_list_default_pagination(rf, mocker):
    """The post list default pagination value is 5."""
    public_mock = mocker.patch('arpegio.blog.views.Post.objects.public')
    mocker.patch('arpegio.blog.views.Post.objects.sticky')
    posts = mommy.prepare(Post, _quantity=20)
    public_mock.return_value = posts
    request = rf.get(reverse('blog:blog'))
    response = PostList.as_view()(request)
    paginator = response.context_data['paginator']
    assert paginator.per_page == 5


def test_post_list_template_name():
    """The template names include 'arpegio-bog/post_list.html'."""
    view = PostList()
    view.object_list = []
    templates = view.get_template_names()
    assert 'arpegio-blog/post_list.html' in templates


def test_post_view(rf, mocker):
    """The post detail view should return a 200 status."""
    get_object_mock = mocker.patch('arpegio.blog.views.PostDetail.get_object')
    post = Post()
    post.slug = 'a-post'
    get_object_mock.return_value = post
    request = rf.get(reverse('blog:post', kwargs={'slug': post.slug}))
    response = PostDetail.as_view()(request, slug=post.slug)
    assert response.status_code == 200


def test_post_view_template_name(mocker):
    """The template names include 'arpegio-blog/post_detail.html'."""
    view = PostDetail()
    view.object = mocker
    templates = view.get_template_names()
    assert 'arpegio-blog/post_detail.html' in templates
