"""Tests for the blog managers."""
from arpegio.blog.models import Post


def test_post_manager_public(mocker):
    """The queryset should filter the public posts."""
    mocker.patch('arpegio.blog.models.PostManager.filter')
    now_mock = mocker.patch('arpegio.blog.managers.now')
    now_mock.return_value = 'now'
    Post.objects.public()
    Post.objects.filter.assert_called_with(status='PB',
                                           creation_date__lt='now'
                                           )


def test_post_manager_sticky(mocker):
    """The queryset should filter the sticky posts."""
    mocker.patch('arpegio.blog.models.PostManager.filter')
    now_mock = mocker.patch('arpegio.blog.managers.now')
    now_mock.return_value = 'now'
    Post.objects.sticky()
    Post.objects.filter.assert_called_with(sticky=True)
