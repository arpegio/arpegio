"""Tests for the Blog's templatetags."""
from model_mommy import mommy

from django.template import Template, Context

from arpegio.blog.templatetags.arpegio_blog_tags import recent_posts
from arpegio.blog.models import Post


def mock_posts(mocker):
    """Mock the Post's objects."""
    posts_mock = mocker.patch(
        'arpegio.blog.templatetags.arpegio_blog_tags.Post.objects.public')
    posts = mommy.prepare(Post, slug='some-slug', _quantity=10)
    posts_mock.return_value = posts
    return posts


def test_recent_posts_renders(mocker):
    """Test the recen_comments templatetags renders."""
    mock_posts(mocker)
    template_snippet = '{% load arpegio_blog_tags %} {% recent_posts %}'
    Template(template_snippet).render(Context({}))


def test_recent_posts_list(mocker):
    """Test the recen_comments posts."""
    posts = mock_posts(mocker)
    expected_posts = posts[:5]
    posts = recent_posts()['posts']
    assert posts == expected_posts


def test_recent_posts_custom_quantity(mocker):
    """Test the recen_comments custom posts number."""
    posts = mock_posts(mocker)
    expected_posts = posts[:6]
    posts = recent_posts(6)['posts']
    assert posts == expected_posts
