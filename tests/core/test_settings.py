"""Test the settings module."""
from arpegio.core.settings import site


def test_register_settings():
    """Test the registration of settings."""
    settings = {'GENERAL': {'Category': {'value': 'Some text'}}}
    expected_settings = {'general': {'category': {'value': 'Some text'}}}
    site.register(settings)
    assert site.settings == expected_settings
