"""Tests for core templatetags."""
from django.template import Template, Context

from arpegio.core.templatetags.arpegio_tags import more
from arpegio.core.templatetags.arpegio_tags import join_link
from arpegio.core.templatetags.arpegio_tags import linebreakshtml


def test_more_templatetag_renders():
    """Test the more templatetag."""
    template_snippet = '{% load arpegio_tags %}{{ text|more }}'
    Template(template_snippet).render(Context({'text': 'Some text'}))


def test_more_templatetag():
    """Test the templatetags splits text."""
    text = """
    <p>This is a paragraph</p><!-- more --><p>This is not shown</p>
    """
    link = '<p><a href="#" class="more-link">Read More</a></p>'
    expected = "<p>This is a paragraph</p>" + link
    assert more(text).strip() == expected.strip()


def test_more_templatetag_several_marks():
    """Test the templatetags only returns the first part."""
    text = """
    <p>This is shown</p><!--more-->
    <p>This is not shown</p><!--more-->
    <p>This is also not shown</p>
    """
    link = '<p><a href="#" class="more-link">Read More</a></p>'
    expected = "<p>This is shown</p>" + link
    assert more(text).strip() == expected.strip()


def test_more_templatetag_no_mark():
    """Test the templatetag does not process text without mark."""
    text = "<p>This text have no mark</p>"
    assert more(text) == text


def test_more_custom_url():
    """Test the templatetags returns a custom url."""
    text = """
    <p>Before link</p><!--   more
    --><p>After link</p>
    """
    url = 'http://www.example.com'
    link = '<p><a href="%s" class="more-link">Read More</a></p>' % url
    expected = "<p>Before link</p>" + link
    assert more(text, url).strip() == expected.strip()


def test_linebreakshtml_renders():
    """Test the linebreakshtml renders."""
    template_snippet = "{% load arpegio_tags %}{{ text|linebreakshtml }}"
    Template(template_snippet).render(Context({'text': 'Some text'}))


def test_linebreakshtml_paragraphs():
    """Test the linebreakshtml paragraphs."""
    text = """
    This is a paragraph.

    This is another paragraph.
    This is inside the same paragraph.
    """
    expected = """<p>This is a paragraph.</p><p>This is another paragraph.<br>
    This is inside the same paragraph.</p>"""
    assert linebreakshtml(text) == expected.replace('\n', '')


def test_linebreakshtml_html():
    """Test the linebreakshtml respects html."""
    text = """
    <h1>This is a heading</h1>

    This is a paragraph.
    This is inside the same paragraph.
    """
    expected = """<h1>This is a heading</h1><p>This is a paragraph.<br>
    This is inside the same paragraph.</p>"""
    assert linebreakshtml(text) == expected.replace('\n', '')


def test_linebreakshtml_html_custom_attrs():  # pylint: disable=invalid-name
    """Test the linebreakshtml respects custom attrs in html."""
    text = """
    <h1 class="custom-class">This is a heading</h1>
    """
    expected = """<h1 class="custom-class">This is a heading</h1>"""
    assert linebreakshtml(text) == expected.replace('\n', '')


class ModelMock(object):  # pylint: disable=too-few-public-methods
    """Mock a model class."""

    @staticmethod
    def get_absolute_url():
        """This mock objects return # as absolute url."""
        return '#'

    def __str__(self):
        return 'Object'

    def __unicode__(self):
        return u'Object'


def test_join_link_renders():
    """Test the join_link renders."""
    mock = ModelMock()
    template_snippet = '{% load arpegio_tags %}{{ mock|join_link }}'
    Template(template_snippet).render(Context({'mock': [mock]}))


def test_join_link_templatetag():
    """Test the join_link templatetag."""
    mocks = []
    expected = ''
    for _ in range(10):
        mocks.append(ModelMock())
        expected = ''.join([expected, '<a href="#">Object</a>'])
    assert join_link(mocks) == expected


def test_join_link_custom_separator():
    """Test the join_link displays a custom separator."""
    mocks = []
    expected = ''
    for _ in range(10):
        mocks.append(ModelMock())
        expected = ', '.join([expected, '<a href="#">Object</a>'])
        assert join_link(mocks, ', ') == expected[2:]
