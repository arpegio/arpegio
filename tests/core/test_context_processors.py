"""Tests the context processors."""
from arpegio.core.context_processors import settings


def test_settings(mocker):
    """Test the returned dictionary."""
    settings_mock = mocker.patch('arpegio.core.context_processors.site')
    settings_mock.settings = {}
    arpegio_settings = settings(None)
    assert arpegio_settings == {'arpegio': {}}
