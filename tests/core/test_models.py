"""Tests for the core models."""
import pytest

from django.template.defaultfilters import slugify

from arpegio.core.models import ContentMixin, Timestampable, Sluggable


def test_content_model_str():
    """String representation of the model should be the title."""
    model = ContentMixin(title='A title')
    assert str(model) == model.title


def test_content_model_str_no_title():
    """If the model has no title it should say '(No title)'."""
    model = ContentMixin()
    assert str(model) == '(No title)'


def test_content_model_slug(mocker):
    """When the model has a title the slug should be the slugified title."""
    mocker.patch('arpegio.core.models.models.Model.save')
    model = ContentMixin(title='A title')
    model.save()
    assert model.slug == slugify(model.title)


def test_content_model_custom_slug(mocker):
    """If the model has a custom slug just ignore the title."""
    mocker.patch('arpegio.core.models.models.Model.save')
    model = ContentMixin(title='A title', slug='custom-slug')
    model.save()
    assert model.slug != slugify(model.title)


def test_content_model_contains_slug():
    """If the model has no title and no custom slug it should raise a
    ValueError exception."""
    model = ContentMixin()
    with pytest.raises(ValueError):
        model.save()


def test_content_model_html_title(mocker):
    """When the title of the object contains HTML the slug should not contain
    it."""
    mocker.patch('arpegio.core.models.models.Model.save')
    model = ContentMixin(title='<h1>HTMl title</h1>')
    model.save()
    assert model.slug == 'html-title'


def test_timestampable_save(mocker):
    """When the model is saved the modification date is updated."""
    mocker.patch('arpegio.core.models.models.Model.save')
    now = mocker.patch('arpegio.core.models.now')
    now.return_value = 'now'
    model = Timestampable()
    model.save()
    assert model.modification_date == 'now'


def test_sluggable_model_slug(mocker):
    """The slug of the model is the slugified name."""
    mocker.patch('arpegio.core.models.models.Model.save')
    model = Sluggable(name='A name')
    model.save()
    assert model.slug == slugify(model.name)
