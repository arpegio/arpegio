from django.apps import AppConfig


class BlogConfig(AppConfig):
    name = 'arpegio.blog'
    label = 'arpegio-blog'
    verbose_name = 'Blog'
