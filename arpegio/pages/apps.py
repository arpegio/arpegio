from django.apps import AppConfig


class PagesConfig(AppConfig):
    name = 'arpegio.pages'
    label = 'arpegio-pages'
    verbose_name = 'Pages'
