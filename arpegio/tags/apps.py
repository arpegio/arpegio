from django.apps import AppConfig


class TagsConfig(AppConfig):
    name = 'arpegio.tags'
    label = 'arpegio-tags'
    verbose_name = 'Tags'
