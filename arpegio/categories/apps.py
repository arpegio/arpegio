from django.apps import AppConfig


class CategoriesConfig(AppConfig):
    name = 'arpegio.categories'
    label = 'arpegio-categories'
    verbose_name = 'Categories'
