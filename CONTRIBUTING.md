# Contributing to Arpegio

Arpegio is open to contributions. There are a few rules to follow. This rules
ensure the quality of the project.

## Submission Guidelines

### Submitting an Issue

Before submitting and issue please be sure to search if there is a issue similar
to yours and comment on it.

If your issue is a bug please provide the following information:

 * Overview of the issue - Here you describe the bug and, if available, the
   stack trace with the error message.

 * Use case - Explain what are you trying to achive and why you consider the
   result a bug.

 * Example - Provide a code example where to reproduce the bug. Please check
   that the bug is triggered by the code and not by your project configuration.

### Submitting a Merge Request

Before submitting a merge request consider:

  * Search open or closed merge request related to yours.

  * Make your changes in a new git branch

  * Create your patch. The testing suit fails if coverage is under 80% so please
    include the relevant test cases or the code won't be merged.

  * Follow the coding standars. If some functionality is included in Django use
    it, don't reinvent the whell. The code is linted with PEP8 and PyLint. Your
    code should pase the linting test.

  * Follow the commit message conventions.

## Commit Guidelines

Each commit message consists of a subject, a body and a footer. The subject has
a special format that includes a type, a scope and a resume:

```
<type>(<scope>): <resume>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

The subject is mandatory and the scope is optional.

Any line of the commit message cannot be longer 80 characters! This allows the
message to be easier to read various git tools.

### Revert

If the commit reverts a previous commit, it should begin with revert:, followed
by the header of the reverted commit. In the body it should say: `This reverts
commit <hash>.`, where the hash is the SHA of the commit being reverted.

### Type

Must be one of the following:

 * FEAT: A new feature
 * FIX: A bug fix
 * DOCS: Documentation only changes
 * STYLE: Changes that do not affect the meaning of the code (white-space,
   formatting, missing semi-colons, etc)
 * REFACTOR: A code change that neither fixes a bug nor adds a feature
 * TEST: Adding missing tests
 * CHORE: Changes to the build process or auxiliary tools and libraries such as
   documentation generation

### Scope

The scope should indicate which app is covered with the commit

### Resume

The resume contains succinct description of the change:

 * Use the imperative, present tense: "change" not "changed" nor "changes"
 * Capitalize first letter
 * Don't end with a dot (.)

### Body

Just as in the subject, use the imperative, present tense: "change" not
"changed" nor "changes". The body should include the motivation for the
change and contrast this with previous behavior.

### Footer

The footer should contain any information about Breaking Changes and is
also the place to reference issues that this commit Closes.

Breaking Changes should start with the word BREAKING CHANGE: with a space
or two newlines. The rest of the commit message is then used for this.
