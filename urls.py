"""URL Configuration."""
from django.conf.urls import url, include

urlpatterns = [
    url(r'^blog/', include('arpegio.blog.urls',
                           namespace='blog',
                           app_name='blog'
                           )
        ),
    url(r'^pages/', include('arpegio.pages.urls',
                            namespace='pages',
                            app_name='pages'
                            )
        ),
    url(r'^category/', include('arpegio.categories.urls',
                               namespace='categories',
                               app_name='categories'
                               )
        ),
    url(r'^tags/', include('arpegio.tags.urls',
                           namespace='tags',
                           app_name='tags'
                           )
        ),
]
